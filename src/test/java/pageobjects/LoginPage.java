package pageobjects;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    @FindBy(xpath = "//div[@class='header_user_info']")
    WebElement signInButton;

    @FindBy(css = "#email")
    WebElement myEmail;

    @FindBy(css = "#passwd")
    WebElement myPassword;

    @FindBy(css = "#SubmitLogin")
    WebElement signInButton2;

    @FindBy(id = "email_create")
    WebElement registerEmailBox;

    @FindBy(id = "SubmitCreate")
    WebElement createAccountButton;

    @FindBy(css = "#SubmitLogin")
    WebElement logIn;

    public LoginPage(WebDriver driverIn, WebDriverWait waitIn){
        super(driverIn, waitIn);
    }

    public void openPage() {

    }

    public void goToRegisterForm(String newEmail) {
        signInButton2.click();
        wait.until(ExpectedConditions.elementToBeClickable(registerEmailBox));
        registerEmailBox.sendKeys(newEmail);
        wait.until(ExpectedConditions.elementToBeClickable(createAccountButton));
        createAccountButton.click();
    }

    public void goToLoginForm(String email){
        signInButton.click();
        wait.until(ExpectedConditions.elementToBeClickable(myEmail));
        myEmail.sendKeys(email);
        myPassword.sendKeys("Kasia");
        logIn.click();
    }
}
