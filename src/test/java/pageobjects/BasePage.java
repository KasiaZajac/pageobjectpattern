package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class BasePage {


    @FindBy(id = "search_query_top")
    WebElement searchBox;

    @FindBy(css = ".menu-content>li>a")
    List<WebElement> productCategories;

    @FindBy(css = ".shopping_cart .ajax_cart_quantity")
    WebElement cartQuantity;

    @FindBy(className = "header_user_info")
    WebElement signInButton;

    WebDriver driver;
    WebDriverWait wait;
    static final String BASE_URL = "http://automationpractice.com/";

    public BasePage(WebDriver driverIn, WebDriverWait waitIn) {
        this.driver = driverIn;
        this.wait = waitIn;
        PageFactory.initElements(driver, this);
    }

    public void searchForProduct(String productName) {
        searchBox.sendKeys("dress");
        driver.findElement(By.id("search_query_top")).sendKeys(Keys.ENTER);
    }

    public void goToProductCategoryByIndex(int productCategoryIndex) {
        productCategories.get(productCategoryIndex).click();
    }

    public void goToSignInPage()  {
        driver.findElement(By.className("header_user_info"));
        signInButton.click();
    }

    public int getCartSize() {
        String carQuantityText = cartQuantity.getText();
        return Integer.parseInt(carQuantityText);
    }
}
