package utils;

import com.github.javafaker.Faker;

public class RandomUser {
    public int gender;
    public String firstName;
    public String lastName;
    public String newEmail;
    public String password;
    public String address1;
    public String address2;
    public String city;
    public String state;
    public String zipcode;
    public int country;
    public int dayOfBirth;
    public int monthOfBirth;
    public int yearOfBirth;
    public int checkBox1;
    public int checkBox2;
    public String homePhone;

    public RandomUser() {
        Faker faker = new Faker();
        gender = faker.random().nextInt(0,1);
        firstName = faker.name().firstName();
        lastName = faker.name().lastName();
        newEmail = firstName + lastName + yearOfBirth + "@gmail.com";
        dayOfBirth = faker.date().birthday().getDay();
        monthOfBirth = faker.date().birthday().getMonth();
        yearOfBirth = faker.date().birthday().getYear();
        checkBox1 = faker.random().nextInt(1);
        checkBox2 = faker.random().nextInt(1);
        password = "xyz" + yearOfBirth;
        city = faker.address().cityName();
        state = faker.address().state();
        zipcode = faker.address().zipCode();
        country = faker.random().nextInt(1);
        address1 = faker.address().streetName();
        address2 = faker.address().buildingNumber();
        homePhone = faker.phoneNumber().subscriberNumber();
    }
}

